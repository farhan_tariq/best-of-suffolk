<?php
const IMPORT_DIRECTORY = __DIR__ . '/../imports';
const EXPORT_DIRECTORY = __DIR__ . '/../exports';

const DUMP_DATE       = '2022-09-22';
const FILTER_BOOKINGS = true;

const BRAND_ID   = 562;
const BRAND_NAME = 'bos';

const FINANCE_CHECK_LIST = [1, 2, 3, 4, 5, 6, 7, 8];

const FINANCIAL_YEAR_START_DATE = '2021-05-01';

const KNOWN_BOOKINGS_FILE = '../known.csv';
const ERROR_CHECK_FILE    = '../error_check.csv';
const DECIMAL_PRECISION   = 2;

const THIRD_PARTY_COMMISSION_AGENTS = [22919, 25516, 25279, 21788, 21803, 29755];
const THIRD_PARTY_UPLIFT_AGENTS     = [26219, 25517, 25280, 24358, 21639, 24542, 29756];

const THIRD_PARTY_AGENTS = [
    22919 => 'Airbnb Commission',
    26219 => 'Airbnb uplift',
    17725 => 'Airbnb',
    23736 => 'API',
    21653 => 'Booking.com - Booking Fee',
    25516 => 'Booking.com commission',
    25279 => 'Booking.com commission',
    21788 => 'Booking.com Standard Rate (15%)',
    21803 => 'Booking.com Standard Rate (22%)',
    25517 => 'Booking.com uplift',
    25280 => 'Booking.com uplift',
    24358 => 'HomeAway uplift',
    21639 => 'HomeAway uplift (6%)',
    24542 => 'HomeAway uplift (6.00%)',
    29755 => 'PlumGuide commission',
    29756 => 'PlumGuide uplift',
];

const HEADERS_FILTERED_BOOKINGS = [
    'Booking Ref',
    'Total Cost',
    'Actual Total Cost',
    'Total Rent',
    'Booking Fee',
    'Extras',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to BOS',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Balance Due on Holiday',
    'SD Amount',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',
    'Holiday start date',
    'Holiday end date',
    'Source',
    'Comm%',
    'Property Comm%',
    'Property Name',
    'Property Unique Ref',
    'Discount',
    'Voucher',
    'Amount Due to Owner (today)',
    'Amount Due to BOS (today)',
    'Cancelled Date',
    'Cancellation Rate',
    'Booking Type',
    'Channel Fee',
    'Charity Donation',
    'Booking Protect Fee',
    'Pet Fee',
    'Cancellation Type',
    'Paid Per Statement',
    'Third Party Identifier',
    'Third Party Commission',
    'Third Party Uplift',
    'Transfer Type',
    'Transfer Booking',
    'Transfer Description',
    'Cancelled Booking Full Rate',
    'Third Party Ref',
    'Deposit Type',
    'Deposit Rate',
    'Deposit Min',
    'Deposit Amount',
    'Balance Amount',
    'Deposit Status',
    'Booking Full Rate',
    'Cash Received Allocated to Agency First',
    'Balance of Cash After Allocation to Agency',
    'Total due to the Home Owner',
    'Balance Remaining to Home Owner After Payments to Date',
    'Debtors Control Account',
    'Debtors2',
    'Bank Current Account',
    'Sykes Income',
    'Sykes Deferred',
    'Owner Deposit',
    'Owner Deferred',
];

const FINANCE_CHECK_HEADERS = [
    'Check 1',
    'Check 2',
    'Check 3',
    'Check 4',
    'Check 5',
    'Check 6',
    'Check 7',
    'Check 8',
    'Check 9',
    'Check 10',
    'Check 11',
    'Check 12',
    'Check 13',
    'Check 14',
    'Check 15',
    'Check 16',
];

const FINANCE_CHECK_HEADER_DESCRIPTIONS = [
    'Check 1' => 'Total Cost - Total Rent - Booking Fee - Extras - Charity Donation',
    'Check 2' => 'Total Cost - Total Due to owner - Total to BOS',
    'Check 3' => 'Total Due to owner - Due to owner EXTRAS - Due to owner RENT',
    'Check 4' => 'Total to BOS - Comm Gross - Booking Fee - Extras Comm Gross ',
    'Check 5' => 'Total Rent - Due to owner RENT - Comm Gross',
    'Check 6' => 'Extras - Due to owner EXTRAS - Extras Comm Gross',
    'Check 7' => 'Total Due to owner - Paid to owner - OWNER LIABILITY',
    'Check 8' => 'Total Cost - Total Relating to Holiday - Balance Due on Holiday',
    'Check 9' => 'Property Comm% - Comm%',
    'Check 10' => 'Paid to owner - Paid Per Statement',
    'Check 11' => 'Debtors Control Account - Bank Current Account - Sykes Income - Sykes Deferred - Owner Deposit - Owner Deferred',
    'Check 12' => 'Total Cost - Debtors Control Account - Bank Current Account',
    'Check 13' => 'Total Due to owner + Owner Deposit + Owner Deferred',
    'Check 14' => 'Balance Due on Holiday - Debtors Control Account',
    'Check 15' => 'Total Relating to Holiday + Sykes Income + Owner Deposit',
    'Check 16' => 'Debtors Control Account + Sykes Deferred + Owner Deferred',
];

const DODGY_BOOKINGS = [
    //    Bookings failing Test 1

    //    Bookings failing Test 2

//    967131,  // Owner overpaid - voucher reduced rent but not owner due
//    974322,  // Owner overpaid - voucher reduced rent but not owner due
//    974372,  // Owner overpaid - voucher reduced rent but not owner due
//    974374,  // Owner overpaid - voucher reduced rent but not owner due
//    974424,  // Owner overpaid - voucher reduced rent but not owner due
//    974442,  // Owner overpaid - voucher reduced rent but not owner due
//    974531,  // Owner overpaid - voucher reduced rent but not owner due
//    974533,  // Owner overpaid - voucher reduced rent but not owner due
//    974535,  // Owner overpaid - voucher reduced rent but not owner due
//    974602,  // Owner overpaid - voucher reduced rent but not owner due
//    974603,  // Owner overpaid - voucher reduced rent but not owner due
//    974741,  // Owner overpaid - voucher reduced rent but not owner due
//    974882,  // Owner overpaid - voucher reduced rent but not owner due
//    974936,  // Owner overpaid - voucher reduced rent but not owner due
//    975304,  // Owner overpaid - voucher reduced rent but not owner due
//    975405,  // Owner overpaid - voucher reduced rent but not owner due
//    975417,  // Owner overpaid - voucher reduced rent but not owner due
//    975422,  // Owner overpaid - voucher reduced rent but not owner due
//    975434,  // Owner overpaid - voucher reduced rent but not owner due
//    975438,  // Owner overpaid - voucher reduced rent but not owner due
//    975453,  // Owner overpaid - voucher reduced rent but not owner due
//    975454,  // Owner overpaid - voucher reduced rent but not owner due
//    975470,  // Owner overpaid - voucher reduced rent but not owner due
//    975490,  // Owner overpaid - voucher reduced rent but not owner due
//    975491,  // Owner overpaid - voucher reduced rent but not owner due
//    975503,  // Owner overpaid - voucher reduced rent but not owner due
//    975507,  // Owner overpaid - voucher reduced rent but not owner due
//    975515,  // Owner overpaid - voucher reduced rent but not owner due
//    975565,  // Owner overpaid - voucher reduced rent but not owner due
//    975570,  // Owner overpaid - voucher reduced rent but not owner due
//    975572,  // Owner overpaid - voucher reduced rent but not owner due
//    975592,  // Owner overpaid - voucher reduced rent but not owner due
//    975638,  // Owner overpaid - voucher reduced rent but not owner due
//    975783,  // Owner overpaid - voucher reduced rent but not owner due
//    975801,  // Owner overpaid - voucher reduced rent but not owner due
//    975808,  // Owner overpaid - voucher reduced rent but not owner due
//    975847,  // Owner overpaid - voucher reduced rent but not owner due
//    975894,  // Owner overpaid - voucher reduced rent but not owner due
//    976005,  // Owner overpaid - voucher reduced rent but not owner due
//    977297,  // Owner overpaid - voucher reduced rent but not owner due
//    978796,  // Owner overpaid - voucher reduced rent but not owner due
//    979285,  // Owner overpaid - voucher reduced rent but not owner due
//    979392,  // Owner overpaid - voucher reduced rent but not owner due
//    979779,  // Owner overpaid - voucher reduced rent but not owner due
//    979909,  // Owner overpaid - voucher reduced rent but not owner due
//    980016,  // Owner overpaid - voucher reduced rent but not owner due
//    980433,  // Owner overpaid - voucher reduced rent but not owner due
//    982126,  // Owner overpaid - voucher reduced rent but not owner due
//    982555,  // Owner overpaid - voucher reduced rent but not owner due
//    983502,  // Owner overpaid - voucher reduced rent but not owner due
//    984307,  // Owner overpaid - voucher reduced rent but not owner due
//    984545,  // Owner overpaid - voucher reduced rent but not owner due
//    984889,  // Owner overpaid - voucher reduced rent but not owner due
//    974916,  // Owner overpaid - voucher reduced rent but not owner due
//    974918,  // Owner overpaid - voucher reduced rent but not owner due
//    975411,  // Owner overpaid - voucher reduced rent but not owner due
//    975418,  // Owner overpaid - voucher reduced rent but not owner due
//    975421,  // Owner overpaid - voucher reduced rent but not owner due
//    975433,  // Owner overpaid - voucher reduced rent but not owner due
//    1005953, // Owner overpaid - voucher reduced rent but not owner due
//    1005384, // Owner overpaid - voucher reduced rent but not owner due
//    1005319, // Owner overpaid - voucher reduced rent but not owner due
//    1005167, // Owner overpaid - voucher reduced rent but not owner due
//    1004867, // Owner overpaid - voucher reduced rent but not owner due
//    1004823, // Owner overpaid - voucher reduced rent but not owner due
//    1004783, // Owner overpaid - voucher reduced rent but not owner due
//    1004449, // Owner overpaid - voucher reduced rent but not owner due
//    1004132, // Owner overpaid - voucher reduced rent but not owner due
//    1004107, // Owner overpaid - voucher reduced rent but not owner due
//    1004015, // Owner overpaid - voucher reduced rent but not owner due
//    1002049, // Owner overpaid - voucher reduced rent but not owner due
//    1001914, // Owner overpaid - voucher reduced rent but not owner due
//    1001503, // Owner overpaid - voucher reduced rent but not owner due
//    1001497, // Owner overpaid - voucher reduced rent but not owner due
//    1001120, // Owner overpaid - voucher reduced rent but not owner due
//    1001014, // Owner overpaid - voucher reduced rent but not owner due
//    999955,  // Owner overpaid - voucher reduced rent but not owner due
//    999751,  // Owner overpaid - voucher reduced rent but not owner due
//    999423,  // Owner overpaid - voucher reduced rent but not owner due
//    999325,  // Owner overpaid - voucher reduced rent but not owner due
//    998937,  // Owner overpaid - voucher reduced rent but not owner due
//    998505,  // Owner overpaid - voucher reduced rent but not owner due
//    998141,  // Owner overpaid - voucher reduced rent but not owner due
//    997882,  // Owner overpaid - voucher reduced rent but not owner due
//    997767,  // Owner overpaid - voucher reduced rent but not owner due
//    997678,  // Owner overpaid - voucher reduced rent but not owner due
//    997058,  // Owner overpaid - voucher reduced rent but not owner due
//    996987,  // Owner overpaid - voucher reduced rent but not owner due
//    996900,  // Owner overpaid - voucher reduced rent but not owner due
//    996861,  // Owner overpaid - voucher reduced rent but not owner due
//    996580,  // Owner overpaid - voucher reduced rent but not owner due
//    996570,  // Owner overpaid - voucher reduced rent but not owner due
//    996263,  // Owner overpaid - voucher reduced rent but not owner due
//    996140,  // Owner overpaid - voucher reduced rent but not owner due
//    996059,  // Owner overpaid - voucher reduced rent but not owner due
//    995925,  // Owner overpaid - voucher reduced rent but not owner due
//    995777,  // Owner overpaid - voucher reduced rent but not owner due
//    995695,  // Owner overpaid - voucher reduced rent but not owner due
//    995383,  // Owner overpaid - voucher reduced rent but not owner due
//    994892,  // Owner overpaid - voucher reduced rent but not owner due
//    994865,  // Owner overpaid - voucher reduced rent but not owner due
//    994842,  // Owner overpaid - voucher reduced rent but not owner due
//    994574,  // Owner overpaid - voucher reduced rent but not owner due
//    994399,  // Owner overpaid - voucher reduced rent but not owner due
//    994397,  // Owner overpaid - voucher reduced rent but not owner due
//    994366,  // Owner overpaid - voucher reduced rent but not owner due
//    994247,  // Owner overpaid - voucher reduced rent but not owner due
//    994028,  // Owner overpaid - voucher reduced rent but not owner due
//    993800,  // Owner overpaid - voucher reduced rent but not owner due
//    993697,  // Owner overpaid - voucher reduced rent but not owner due
//    993662,  // Owner overpaid - voucher reduced rent but not owner due
//    993632,  // Owner overpaid - voucher reduced rent but not owner due
//    993586,  // Owner overpaid - voucher reduced rent but not owner due
//    993405,  // Owner overpaid - voucher reduced rent but not owner due
//    993372,  // Owner overpaid - voucher reduced rent but not owner due
//    993204,  // Owner overpaid - voucher reduced rent but not owner due
//    993078,  // Owner overpaid - voucher reduced rent but not owner due
//    993029,  // Owner overpaid - voucher reduced rent but not owner due
//    992963,  // Owner overpaid - voucher reduced rent but not owner due
//    992958,  // Owner overpaid - voucher reduced rent but not owner due
//    992937,  // Owner overpaid - voucher reduced rent but not owner due
//    992901,  // Owner overpaid - voucher reduced rent but not owner due
//    992639,  // Owner overpaid - voucher reduced rent but not owner due
//    992614,  // Owner overpaid - voucher reduced rent but not owner due
//    992511,  // Owner overpaid - voucher reduced rent but not owner due
//    992506,  // Owner overpaid - voucher reduced rent but not owner due
//    992401,  // Owner overpaid - voucher reduced rent but not owner due
//    992294,  // Owner overpaid - voucher reduced rent but not owner due
//    991536,  // Owner overpaid - voucher reduced rent but not owner due
//    991374,  // Owner overpaid - voucher reduced rent but not owner due
//    991372,  // Owner overpaid - voucher reduced rent but not owner due
//    991272,  // Owner overpaid - voucher reduced rent but not owner due
//    990655,  // Owner overpaid - voucher reduced rent but not owner due
//    990570,  // Owner overpaid - voucher reduced rent but not owner due
//    990221,  // Owner overpaid - voucher reduced rent but not owner due
//    989275,  // Owner overpaid - voucher reduced rent but not owner due
//    989100,  // Owner overpaid - voucher reduced rent but not owner due
//    988162,  // Owner overpaid - voucher reduced rent but not owner due
//    988081,  // Owner overpaid - voucher reduced rent but not owner due
//    987927,  // Owner overpaid - voucher reduced rent but not owner due
//    987443,  // Owner overpaid - voucher reduced rent but not owner due
//    987439,  // Owner overpaid - voucher reduced rent but not owner due
//    986998,  // Owner overpaid - voucher reduced rent but not owner due
//    986796,  // Owner overpaid - voucher reduced rent but not owner due
//    986502,  // Owner overpaid - voucher reduced rent but not owner due
//    986356,  // Owner overpaid - voucher reduced rent but not owner due
//    981637,  // Owner overpaid - voucher reduced rent but not owner due
//    980934,  // Owner overpaid - voucher reduced rent but not owner due
//    980322,  // Owner overpaid - voucher reduced rent but not owner due
//    979414,  // Owner overpaid - voucher reduced rent but not owner due
//    979092,  // Owner overpaid - voucher reduced rent but not owner due
//    978739,  // Owner overpaid - voucher reduced rent but not owner due
//    978325,  // Owner overpaid - voucher reduced rent but not owner due
//    977428,  // Owner overpaid - voucher reduced rent but not owner due
//    976648,  // Owner overpaid - voucher reduced rent but not owner due
//    976356,  // Owner overpaid - voucher reduced rent but not owner due
//    975805,  // Owner overpaid - voucher reduced rent but not owner due
//    975754,  // Owner overpaid - voucher reduced rent but not owner due
//    975642,  // Owner overpaid - voucher reduced rent but not owner due
//    975640,  // Owner overpaid - voucher reduced rent but not owner due
//    975612,  // Owner overpaid - voucher reduced rent but not owner due
//    975589,  // Owner overpaid - voucher reduced rent but not owner due
//    975564,  // Owner overpaid - voucher reduced rent but not owner due
//    975529,  // Owner overpaid - voucher reduced rent but not owner due
//    975524,  // Owner overpaid - voucher reduced rent but not owner due
//    975508,  // Owner overpaid - voucher reduced rent but not owner due
//    975504,  // Owner overpaid - voucher reduced rent but not owner due
//    975449,  // Owner overpaid - voucher reduced rent but not owner due
//    975445,  // Owner overpaid - voucher reduced rent but not owner due
//    977266,  // Owner overpaid - voucher reduced rent but not owner due
//    975915,  // Owner overpaid - voucher reduced rent but not owner due
//    979566,  // Owner overpaid - voucher reduced rent but not owner due
//    979029,  // Owner overpaid - voucher reduced rent but not owner due
//    984164,  // airbnb uplift reducing the rent and total price
//    968288,  // Owner overpaid - voucher reduced rent but not owner due - not fully matching the voucher amount
//    1000837, // Does not add up


    //    Bookings failing Test 3

    //    Bookings failing Test 4

    //    Bookings failing Test 5

    //    Bookings failing Test 6

    //    Bookings failing Test 7

    //    Bookings failing Test 8
];