<?php
include_once 'dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();


function array_stripos($haystack, $needles): bool
{
    $needles = (array) $needles;
    foreach ($needles as $needle) {
        if (stripos($haystack, $needle) !== false) {
            return true;
        }
    }

    return false;
}


function debugBooking(object $booking, int $bookingId, array $fields, bool $die = false): void
{
    if ((int) $booking->__pk === $bookingId) {
        print_r($fields);

        if ($die) {
            die('Finished debugBooking()');
        }
    }
}

function stringArrayToLowerCase($array)
{
    $data = [];
    foreach ($array as $key => $item) {
        $data[$key] = strtolower($item);
    }

    return $data;
}

function initDB()
{
    $servername = getenv('DB_SERVERNAME');
    $username   = getenv('DB_USERNAME');
    $password   = getenv('DB_PASSWORD');
    $database   = getenv('DB_NAME');

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    echo 'Connected' . PHP_EOL;
    return $conn;
}

function closeDB($link)
{
    mysqli_close($link);
}

function openFile($directory, $filename, $mode)
{
    if (! file_exists($directory)) {
        mkdir($directory, 0777, true);
    }

    $out = fopen($directory . $filename, $mode);

    if (! $out) {
        die('Could not open file: "' . $filename . '"');
    }

    return $out;
}

function addDescriptions($out, $headers, $counter)
{
    $descriptions = [];
    for ($i = 0; $i < $counter; $i++) {
        $descriptions[] = '';
    }

    fputcsv($out, array_merge($descriptions, $headers));
}

function addHeaders($out, $reportHeaders, $checkHeaders)
{
    fputcsv($out, array_merge($reportHeaders, $checkHeaders));
}

function roundIt($num): float
{
    return round($num, DECIMAL_PRECISION);
}
