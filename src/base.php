<?php

use Carbon\Carbon;

include_once 'Constants.php';
include_once 'FinanceChecks.php';
include_once 'Helpers.php';
include_once 'repository.php';
require '../vendor/autoload.php';

$conn          = initDB();
$financeChecks = new FinanceChecks();
$actualDumpDate = (new Carbon(DUMP_DATE))->subDay()->toDateString();

$outputFileName = FILTER_BOOKINGS ? 'filtered-booking-' . $actualDumpDate . '-Dump-' . strtolower(BRAND_NAME) . '.csv' : 'filtered-booking-' . $actualDumpDate . '-Dump-' . strtolower(BRAND_NAME) . '.csv';
$out            = null;

if (empty($argv[1])) {
    $out = openFile(EXPORT_DIRECTORY . '/' . $actualDumpDate . '/', $outputFileName, 'wb');
    addDescriptions($out, FINANCE_CHECK_HEADER_DESCRIPTIONS, 57);
    addHeaders($out, HEADERS_FILTERED_BOOKINGS, FINANCE_CHECK_HEADERS);
}

$knownFile = openFile(IMPORT_DIRECTORY . '/', KNOWN_BOOKINGS_FILE, 'rb');
$headers   = fgetcsv($knownFile);
$known     = [];

while (($row = fgetcsv($knownFile)) !== false) {
    $row               = array_combine($headers, $row);
    $known[$row['Booking Ref']] = $row;
}

$bookings              = getAllBookings($conn, $argv);
$securityDeposits      = getAllSecurityDeposits($conn, $bookings, $argv);
$customerPayments      = getCustomerPayments($conn, $bookings, $securityDeposits, $argv);
$thirdPartyCommissions = getThirdPartyAgentFees($conn, $bookings, $argv);
$bookingsExtras        = getAllExtras($conn, $bookings, $argv);
$ownerPayments         = getAllOwnerPayments($conn, $bookings, $argv);
