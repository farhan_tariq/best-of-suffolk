<?php

use Carbon\Carbon;

require_once 'base.php';

foreach ($bookings as $booking) {
    if (filterBooking($booking, FILTER_BOOKINGS)) {
        continue;
    }

    $extras                = getBookingExtrasTotals($booking);
    $source                = $booking->source;
    $thirdPartyUplift      = $booking->third_party_uplift;
    $thirdPartyCommission  = $booking->third_party_commission;
    $thirdPartyDescription = substr($booking->third_party_description, 0, -4);
    $totalCost             = getBookingTotalPrice($booking, $extras) - $thirdPartyCommission;
    $totalCost             -= stripos($thirdPartyDescription, 'booking.com') !== false ? $thirdPartyUplift : 0;

    $rentalPrice           = $booking->rental_price - $thirdPartyCommission;
    $rentalPrice           -= stripos($thirdPartyDescription, 'booking.com') !== false ? $thirdPartyUplift : 0;
    $booking->extras_total = $extrasTotal = $extras->brandExtras + $extras->bookingProtectFee + $extras->ownerExtras;

    $sdPaid     = $booking->sd_paid_amount;
    $sdRefunded = abs($booking->sd_refund_amount);
    $sdHeld     = $sdPaid > 0 && $sdRefunded == 0 ? $sdPaid : roundIt($sdPaid - $sdRefunded);

    $totalReceivedFromCustomer       = roundIt($booking->total_received_from_customer - $booking->total_refunded_to_customer) + $booking->voucher + $sdHeld;
    $totalRelatingToHoliday          = $totalReceivedFromCustomer - $sdHeld;
    $booking->balance_due_on_holiday = $balanceDueOnHoliday = roundIt($totalCost - $totalRelatingToHoliday);

    $totalDueToOwner = $booking->due_to_owner === '0.00' ? getTotalDueToOwner($booking, $rentalPrice) : $booking->due_to_owner;
    $dueToOwnerRent  = $booking->due_to_owner === '0.00' ? $totalDueToOwner - $extras->ownerExtras : $booking->due_to_owner - $extras->ownerExtras;

    $booking->calculated_total_rent = $rentalPrice;

    $depositAmount = $booking->deposit_amount > 0 ? $booking->deposit_amount - $booking->booking_fee - $extras->bookingProtectFee : $booking->deposit_amount;

    $cashReceivedAllocatedToAgencyFirst         = $booking->booking_fee + (($totalRelatingToHoliday - $booking->booking_fee) * ($booking->commission_rate));
    $balanceOfCashAfterAllocationLeftForOwner   = $totalRelatingToHoliday - $cashReceivedAllocatedToAgencyFirst;
    $balanceRemainingToOwnerAfterPaymentsToDate = $balanceOfCashAfterAllocationLeftForOwner - $totalDueToOwner;

    $debtorsControlAccount = $balanceDueOnHoliday;
    $debtors2              = 0;
    $bankCurrentAccount    = $totalRelatingToHoliday;
    $ownerDeposits         = round($balanceOfCashAfterAllocationLeftForOwner * -1, 2);
    $sykesIncome           = round(($bankCurrentAccount * -1) - $ownerDeposits, 2);
    $ownerDeferred         = round(($totalDueToOwner * -1) - $ownerDeposits, 2);
    $sykesDeferred         = round((getTotalToBOS($booking, $extras) * -1) - $sykesIncome, 2);

    $row = [
        $booking->id,                                                                          // 'Booking Ref'
        $totalCost,                                                                            // 'Total Cost'
        $booking->total_price,                                                                 // 'Actual Total Cost'
        roundIt($rentalPrice),                                                                 // 'Total Rent'
        $booking->booking_fee,                                                                 // 'Booking Fee'
        $extrasTotal,                                                                          // 'Extras'
        $dueToOwnerRent,                                                                       // 'Due to owner RENT'
        $extras->ownerExtras,                                                                  // 'Due to owner EXTRAS'
        $totalDueToOwner,                                                                      // 'Total Due to owner'
        $booking->paid_to_owner,                                                               // 'Paid to owner'
        roundIt($totalDueToOwner - $booking->paid_to_owner),                              // 'OWNER LIABILITY'
        getTotalToBOS($booking, $extras),                                                      // 'Total to BOS'
        getCommissionGross($booking),                                                          // 'Comm Gross'
        getCommissionNet($booking),                                                            // 'Comm Net'
        getCommissionVat($booking),                                                            // 'Comm VAT'
        getExtrasCommGross($booking, $extras),                                                 // 'Extras Comm Gross'
        getExtrasCommNet($booking, $extras),                                                   // 'Extras Comm Net'
        getExtrasCommVAT($booking, $extras),                                                   // 'Extras Comm VAT'
        $totalReceivedFromCustomer,                                                            // 'Total Received From Customer'
        $totalRelatingToHoliday,                                                               // 'Total Relating to Holiday'
        $balanceDueOnHoliday,                                                                  // 'Balance Due on Holiday'
        $booking->sd_amount == 0 ? $booking->security_deposit_amount : $booking->sd_amount,    // 'SD Amount'
        $sdPaid,                                                                               // 'SD Paid'
        $sdRefunded,                                                                           // 'SD Refunded'
        $sdHeld,                                                                               // 'Balance of SD Held'
        $booking->status,                                                                      // 'Status'
        $booking->_fk_property,                                                                // 'Property ref'
        $booking->_fk_owner,                                                                   // 'Owner ref'
        $booking->booked_date,                                                                 // 'Holiday booked date'
        $booking->from_date,                                                                   // 'Holiday start date'
        $booking->to_date,                                                                     // 'Holiday end date'
        $booking->source,                                                                      // 'Source'
        getPropertyCommissionPercentage($booking) * 100,                                       // 'Comm%'
        $booking->property_commission_percentage,                                              // 'Property Comm%'
        $booking->property_name,                                                               // 'Property Name'
        $booking->property_unique_ref,                                                         // 'Property Unique Ref'
        $booking->discount,                                                                    // 'Discount'
        $booking->voucher,                                                                     // 'Voucher'
        $booking->due_to_owner,                                                                // 'Amount Due to Owner (today)'
        'Amount Due to BOS (today)',                                                           // 'Amount Due to BOS (today)'
        $booking->cancelled_date,                                                              // 'Cancelled Date'
        $booking->cancelled_bookingfullrate,                                                   // 'Cancellation Rate'
        $booking->booking_type,                                                                // 'Booking Type'
        $booking->channel_fee_value * (($booking->channel_fee_vat / 100) + 1),                 // 'Channel Fee'
        $extras->donation,                                                                     // 'Charity Donation'
        $extras->bookingProtectFee,                                                            // 'Booking Protect Fee'
        getPetFee($booking),                                                                   // 'Pet Fee'
        getCancellationNote($booking),                                                         // 'Cancellation Type'
        getPaidPerStatement($booking),                                                         // 'Paid Per Statement'
        $thirdPartyDescription,                                                                // 'Third Party Identifier'
        $thirdPartyCommission,                                                                 // 'Third Party Commission'
        $thirdPartyUplift,                                                                     // 'Third Party Uplift'
        substr($booking->transfer_type, 0, -4),                                   // 'Transfer Type'
        substr($booking->transfer_booking, 0, -4),                                // 'Transfer Booking'
        substr($booking->transfer_description, 0, -4),                            // 'Transfer Description'
        $booking->cancelled_bookingfullrate,                                                   // 'Cancelled Booking Full Rate'
        $booking->third_party_ref,                                                             // 'Third Party Ref'
        $booking->deposit_type,                                                                // 'Deposit Type'
        $booking->deposit_rate,                                                                // 'Deposit Rate'
        $booking->deposit_min,                                                                 // 'Deposit Min'
        $depositAmount,                                                                        // 'Deposit Amount'
        $booking->balance_amount,                                                              // 'Balance Amount'
        getDepositStatus($booking),                                                            // 'Deposit Status'
        $booking->bookingfullrate,                                                             // 'Booking Full Rate'
        $cashReceivedAllocatedToAgencyFirst,                                                   // 'Cash Received Allocated to Agency First'
        $balanceOfCashAfterAllocationLeftForOwner,                                             // 'Balance of Cash After Allocation to Agency'
        $totalDueToOwner,                                                                      // 'Total due to the Home Owner'
        $balanceRemainingToOwnerAfterPaymentsToDate,                                           // 'Balance Remaining to Home Owner After Payments to Date'
        $debtorsControlAccount,                                                                // 'Debtors Control Account'
        $debtors2,                                                                             // 'Debtors2'
        $bankCurrentAccount,                                                                   // 'Bank Current Account'
        $sykesIncome,                                                                          // 'Sykes Income'
        $sykesDeferred,                                                                        // 'Sykes Deferred'
        $ownerDeposits,                                                                        // 'Owner Deposit'
        $ownerDeferred,                                                                        // 'Owner Deferred'
    ];

    $row = array_combine(HEADERS_FILTERED_BOOKINGS, $row);

    $financeChecks->printBooking($argv, $booking, $row);
    $financeChecks->runFinanceChecks($argv, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    $financeChecks->sumFinanceChecks($row);
    $financeChecks->checkBooking($known, $booking, $row);

    $financeChecksResults = $financeChecks->getBookingsErrorCheck()[$booking->id];
    $financeChecksRow     = array_combine(FINANCE_CHECK_HEADERS, (array) $financeChecksResults);
    $row                  = array_merge($row, $financeChecksRow);

    if (empty($argv[1])) {
        fputcsv($out, $row);
    }
}

if (empty($argv[1])) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;
    $financeChecks->compareFinanceChecks();
    $financeChecks->appendDataToErrorCheck(ERROR_CHECK_FILE);
}

//$financeChecks->outputErrorReport('check2');

function getTotalDueToOwner($booking, $rentalPrice): float
{
    $commission = ($rentalPrice * ($booking->property_commission_percentage / 100)) * 1.2;
    return $rentalPrice - $commission;
}

function getTotalToBOS($booking, $extras): float
{
    return roundIt(getBookingCommission($booking)) + $booking->booking_fee + $extras->bookingProtectFee + $extras->brandExtras + $extras->donation;
}

function getCommissionNet($booking): float
{
    return roundIt(getBookingCommission($booking) / 1.2);
}

function getCommissionVAT($booking): float
{
    return roundIt(getCommissionNet($booking) * 0.2);
}

function getCommissionGross($booking)
{
    return getCommissionNet($booking) + getCommissionVat($booking);
}

function getExtrasCommNet($booking, $extras): float
{
    return roundIt(($extras->brandExtras + $extras->bookingProtectFee) / 1.2);
}

function getExtrasCommVAT($booking, $extras): float
{
    $extrasCommNet = getExtrasCommNet($booking, $extras);

    return roundIt($extrasCommNet * 1.2 - $extrasCommNet);
}

function getPaidPerStatement($booking): float
{
    $total = 0;
    foreach ($booking->owner_payments as $ownerPayment) {
        if ($ownerPayment->paid_date !== '0000-00-00 00:00:00') {
            $total += $ownerPayment->amount;
        }
    }

    return $total;
}

function getExtrasCommGross($booking, $extras): float
{
    return roundIt(getExtrasCommNet($booking, $extras) + getExtrasCommVAT($booking, $extras));
}

function filterBooking($booking, $filter): bool
{
    $filterDate = new Carbon('2022-05-01');
    $fromDate   = new Carbon($booking->from_date);

    return $fromDate < $filterDate && $filter;
    //    $booking->booking_type === 'OWNER' || $booking->status === 'cancelled';
}

function getPropertyCommissionPercentage($booking)
{
    $propertyCommissionPercentage = $booking->property_commission_percentage;
    $rentalPrice                  = $booking->rental_price;

    $thirdPartyCommission = $booking->third_party_commission;

    $rentalPrice -= $thirdPartyCommission;

    $bookingCommissionWithVAT = getBookingCommission($booking);
    $bookingCommissionNet     = roundIt($bookingCommissionWithVAT / 1.2);

    if ($rentalPrice > 0) {
        if (roundIt($bookingCommissionNet / $rentalPrice) == roundIt((float) $propertyCommissionPercentage / 100)) {
            return $propertyCommissionPercentage / 100;
        }
        return $bookingCommissionNet / $rentalPrice;
    }
    return $propertyCommissionPercentage / 100;
}

function getBookingCommission($booking)
{
    $total = 0;
    foreach ($booking->owner_payments as $owner_payment) {
        if (array_stripos($owner_payment->payment_caption, ['bunk', 'linen']) === false) {
            $total += $owner_payment->commission_amount;
        }
    }

    if (($total == 0 && $booking->due_to_owner === '0.00') || empty($booking->owner_payments)) {
        $total = ($booking->calculated_total_rent * ($booking->property_commission_percentage / 100)) * 1.2;
    }

    return $total;
}

function getBookingExtrasTotals($booking)
{
    $brandExtras       = 0;
    $ownerExtras       = 0;
    $bookingProtectFee = 0;
    $donation          = 0;

    $bookingProtectItems = ['protect', 'bp cover', 'bp charge', 'refunded', 'bp cancel', 'cancel bp', 'bp refund', 'bp canx', 'bp cover'];

    foreach ($booking->extras as $extra) {
        if (array_stripos($extra->description, ['Booking Fee']) !== false) {
            continue;
        }

        if (array_stripos($extra->description, $bookingProtectItems)) {
            $bookingProtectFee += $extra->total_price;
            continue;
        }

        if (array_stripos($extra->description, ['donation']) !== false) {
            $donation += $extra->total_price;
            continue;
        }

        if ($extra->total_price - $extra->ownerprice == 0) {
            $ownerExtras += $extra->total_price;
            continue;
        }

        if ($extra->total_price - $extra->ownerprice == $extra->total_price) {
            $brandExtras += $extra->total_price;
            continue;
        }

        if ($extra->total_price - $extra->ownerprice != $extra->total_price && $extra->ownerprice != 0) {
            $brandExtras += ($extra->total_price - $extra->ownerprice) * 1.2;
            $ownerExtras += $extra->total_price - (($extra->total_price - $extra->ownerprice) * 1.2);
        }
    }

    return (object) [
        'bookingProtectFee' => $bookingProtectFee,
        'donation' => $donation,
        'brandExtras' => $brandExtras,
        'ownerExtras' => $ownerExtras,
    ];
}

function getBookingTotalPrice($booking, $extras)
{
    $fullRate    = ! empty($booking->bookingfullrate) && $booking->bookingfullrate > $booking->rental_price ? $booking->bookingfullrate : $booking->rental_price;
    $bookingFee  = $booking->booking_fee;
    $voucher     = 0;
    $discount    = $booking->discount;
    $extrasTotal = $extras->brandExtras + $extras->ownerExtras + $extras->bookingProtectFee + $extras->donation;

    return $fullRate + $bookingFee - $voucher - $discount + $extrasTotal;
}

function getPetFee($booking): float
{
    $total = 0;

    foreach ($booking->extras as $extra) {
        if (array_stripos($extra->description, ['dog', 'pet']) !== false) {
            $total += $extra->total_price;
        }
    }

    return $total;
}

function getCancellationNote($booking): string
{
    return trim(preg_replace('/\s\s+/', ' ', strip_tags($booking->cleanersnotes)));
}

function getDepositStatus($booking): string
{
    $expectedDeposit = roundIt(getExpectedDeposit($booking));

    if ($booking->balance_due_on_holiday <= 0) {
        return 'No Balance Due';
    }

    if (
        $booking->deposit_rate == $booking->deposit_amount ||
        $booking->deposit_amount - $booking->booking_fee == $booking->deposit_rate
    ) {
        return 'Default Low Deposit';
    }

    if ($booking->deposit_amount == $expectedDeposit) {
        return 'Newly created - 25% plus Bkg Fee plus Extras';
    }

    if ($booking->deposit_amount < $booking->deposit_rate) {
        return 'Newly created - < Default';
    }

    if ($booking->deposit_amount < $expectedDeposit) {
        return 'Newly Created - < Std Terms';
    }

    if ($booking->deposit_amount > $expectedDeposit) {
        return 'Newly Created > Std Terms';
    }

    return 'Other';
}

function getExpectedDeposit($booking): float
{
    if (strtolower($booking->status) === 'confirmed' && $booking->balance_due_on_holiday <= 0) {
        return 0;
    }

    if (strtolower($booking->status) === 'confirmed' && $booking->balance_due_on_holiday > 0) {
        return (0.25 * $booking->calculated_total_rent) + $booking->booking_fee + $booking->extras_total;
    }

    return (0.25 * $booking->cancelled_bookingfullrate) + $booking->booking_fee + $booking->extras_total;
}
