<?php

function getAllBookings($conn, $argv)
{
    $bookings = [];
    $where    = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
            SELECT `b`.`booking_id` AS id,
                   `b`.`_fk_property`,
                   `b`.`_fk_customer`,
                   `b`.`booked_date`,
                   `b`.`from_date`,
                   `b`.`to_date`,
                   `b`.`status`,
                   `b`.source,
                   `b`.`booking_type`,
                   `b`.`total_price`,
                   `bd`.bookingfullrate,
                   `b`.`rental_price`,
                   `bd`.`bookingdiscount_couples` + `bd`.bookingdiscount_early + `bd`.bookingdiscount_late + `bd`.bookingdiscount_multi + `bd`.bookingdiscount_oap + `bd`.bookingdiscount_spare + `bd`.bookingdiscount_xy + `bd`.bookingfixeddiscount + `bd`.bookingotherdiscount AS discount,
                   `b`.`voucher`,
                   `b`.`booking_fee_price` AS booking_fee,
                   `b`.`credit_card_fee_amount`,
                   `b`.`extras_price`,
                   `b`.`commission_rate`,
                   `b`.`due_to_owner`,
                   `b`.`paid_to_owner`,
                   `b`.`outstanding_to_owner`,
                   `b`.`deposit_amount`,
                   `b`.`balance_amount`,
                   `b`.`security_deposit_amount`,
                   `b`.`security_deposit_paid`,
                   `b`.`security_deposit_due_date`,
                   `b`.`cancelled_date`,
                   `b`.`channel_fee_value`,
                   `b`.`channel_fee_vat`,
                   `bd`.cancelled_bookingfullrate,
                   `p`.commission_percentage AS property_commission_percentage,
                   `p`.name AS property_name,
                   `p`.rcvoldID AS property_unique_ref,
                   `p`._fk_owner,
                   `bd`.cleanersnotes,
                   `b`.third_party_ref,
                   `p`.deposit_type,
                   `p`.deposit_rate,
                   `p`.deposit_min
            FROM `booking` b
                     JOIN `booking_details` `bd` ON `b`.__pk = bd.bookingID
                     JOIN property p ON p.__pk = b._fk_property   
            WHERE b.from_date >= '" . FINANCIAL_YEAR_START_DATE . "' {$where} ORDER BY b.booking_id ;";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            $bookings[$obj->id]                               = $obj;
            $bookings[$obj->id]->total_received_from_customer = 0;
            $bookings[$obj->id]->total_refunded_to_customer   = 0;
            $bookings[$obj->id]->sd_refund_amount             = 0;
            $bookings[$obj->id]->sd_amount                    = 0;
            $bookings[$obj->id]->sd_paid_amount               = 0;
            $bookings[$obj->id]->calculated_total_rent        = 0;
            $bookings[$obj->id]->transfer_type                = '';
            $bookings[$obj->id]->transfer_booking             = '';
            $bookings[$obj->id]->transfer_description         = '';
            $bookings[$obj->id]->payments                     = [];
            $bookings[$obj->id]->third_party_fees             = [];
            $bookings[$obj->id]->third_party_commission       = 0;
            $bookings[$obj->id]->third_party_uplift           = 0;
            $bookings[$obj->id]->third_party_description      = '';
            $bookings[$obj->id]->extras                       = [];
            $bookings[$obj->id]->owner_payments               = [];
        }
    }
    echo "Fetched All Bookings" . PHP_EOL;
    return $bookings;
}

function getCustomerPayments($conn, $bookings, $securityDeposits, $argv): array
{
    $payments = [];
    $where    = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
            SELECT bp.*
            FROM booking b
                JOIN booking_payment bp ON bp.`_fk_booking` = b.`booking_id`
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {

            if (! empty($bookings[$obj->_fk_booking])) {
                $bookingSecurityDeposits = ! empty($securityDeposits[$obj->_fk_booking]) ? $securityDeposits[$obj->_fk_booking] : [];

                foreach ($bookingSecurityDeposits as $securityDeposit) {
                    if ($securityDeposit->payment_id > 0) {
                        if ($securityDeposit->payment_id === $obj->__pk) {
                            if ($securityDeposit->amount === $obj->amount) {
                                continue 2;
                            }

                            if ($securityDeposit->amount < $obj->amount) {
                                $obj->amount -= $securityDeposit->amount;
                                break;
                            }

                            if (
                                $securityDeposit->amount < 0 && $obj->amount < 0 &&
                                abs($securityDeposit->amount) < abs($obj->amount)
                            ) {
                                $obj->amount -= $securityDeposit->amount;
                                break;
                            }
                        }
                    }

                    if ($securityDeposit->payment_id < 0) {
                        if ($securityDeposit->amount === $obj->amount) {
                            continue 2;
                        }
                    }
                }

                if ($obj->amount > 0) {
                    $bookings[$obj->_fk_booking]->total_received_from_customer += $obj->amount;
                }

                if ($obj->amount < 0) {
                    $bookings[$obj->_fk_booking]->total_refunded_to_customer += $obj->amount * -1;
                }

                $transferDetails = getBookingTransferDetail($obj->paymentcaption);

                $bookings[$obj->_fk_booking]->payments[]           = $obj;
                $bookings[$obj->_fk_booking]->transfer_type        .= ! empty($transferDetails['transfer_type']) ? $transferDetails['transfer_type'] . ' -- ' : '';
                $bookings[$obj->_fk_booking]->transfer_booking     .= ! empty($transferDetails['transfer_booking']) ? $transferDetails['transfer_booking'] . ' -- ' : '';
                $bookings[$obj->_fk_booking]->transfer_description .= ! empty($transferDetails['transfer_description']) ? $transferDetails['transfer_description'] . ' -- ' : '';
            }
            $payments[$obj->_fk_booking][] = $obj;
        }
    }

    echo "Fetched All Payments" . PHP_EOL;
    return $payments;
}

function getThirdPartyAgentFees($conn, $bookings, $argv): array
{
    $thirdPartyFees = [];
    $where          = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
            SELECT bpa.* FROM booking b 
                JOIN booking_payments_agents bpa ON bpa.`owner_bookingID` = b.`booking_id`
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->owner_bookingID])) {
                if (in_array($obj->agentID, THIRD_PARTY_COMMISSION_AGENTS)) {
                    $bookings[$obj->owner_bookingID]->third_party_commission += $obj->commission;
                }

                if (in_array($obj->agentID, THIRD_PARTY_UPLIFT_AGENTS)) {
                    $bookings[$obj->owner_bookingID]->third_party_uplift += $obj->commission;
                }

                $bookings[$obj->owner_bookingID]->third_party_description .= THIRD_PARTY_AGENTS[$obj->agentID] . ' -- ';
                $bookings[$obj->owner_bookingID]->third_party_fees[]      = $obj;
            }
            $thirdPartyFees[$obj->owner_bookingID][] = $obj;
        }
    }

    echo "Fetched All Third Party Commissions" . PHP_EOL;
    return $thirdPartyFees;
}

function getAllExtras($conn, $bookings, $argv): array
{
    $extras = [];
    $where  = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
            SELECT b.booking_id AS id, bi.* 
            FROM booking_item bi 
                JOIN booking b on bi._fk_booking_detail_id = b.booking_detail_id
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->id])) {
                $bookings[$obj->id]->extras[] = $obj;
            }
            $extras[$obj->id][] = $obj;
        }
    }

    echo "Fetched All Bookings Extras" . PHP_EOL;
    return $extras;
}

function getAllOwnerPayments($conn, $bookings, $argv): array
{
    $ownerPayments = [];
    $where         = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
           SELECT b.booking_id AS id, opo.* 
           FROM owner_payment_owe opo 
            JOIN booking b ON b.__pk = opo.booking_id
           WHERE b.from_date >= '" . FINANCIAL_YEAR_START_DATE . "'  {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->id])) {
                $bookings[$obj->id]->owner_payments[] = $obj;
            }
            $ownerPayments[$obj->id][] = $obj;
        }
    }

    echo "Fetched All Owner Payments" . PHP_EOL;
    return $ownerPayments;
}

function getAllSecurityDeposits($conn, array $bookings, $argv): array
{
    $securityDeposits = [];
    $where            = '';

    if (! empty($argv[1]) && ! in_array($argv[1], FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . $argv[1];
    }

    $sql = "
           SELECT b.booking_id,sd.amount, sd.date, sd.payment_id, sd.payment_type, sd.suspend_refund FROM booking_security_deposit sd 
            JOIN booking b ON b.__pk = sd.booking_id
           WHERE b.from_date >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->booking_id])) {
                if ($obj->amount > 0) {
                    $bookings[$obj->booking_id]->sd_paid_amount += $obj->amount;
                    $bookings[$obj->booking_id]->sd_amount      += $obj->amount;
                } else {
                    $bookings[$obj->booking_id]->sd_refund_amount += $obj->amount < 0 ? $obj->amount * -1 : $obj->amount;
                }
            }

            $securityDeposits[$obj->booking_id][] = $obj;
        }
    }

    echo "Fetched All Bookings Security Deposits" . PHP_EOL;
    return $securityDeposits;
}

function getBookingTransferDetail($paymentCaption): array
{
    $regex = "/(.*tfr'd\s+?(to)\s+?(\d{6,7}))|(.*tfr'd\s+?(from)\s+?(\d{6,7}))/m";

    preg_match($regex, $paymentCaption, $matches);

    return [
        'transfer_type' => ! empty($matches[2]) ? $matches[2] : (! empty($matches[5]) ? $matches[5] : ''),
        'transfer_booking' => ! empty($matches[3]) ? $matches[3] : (! empty($matches[6]) ? $matches[6] : ''),
        'transfer_description' => ! empty($matches[0]) ? $matches[0] : '',
    ];
}